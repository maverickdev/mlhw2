import numpy as np
import pandas as pd
import scipy as sp
import math
import collections
from sklearn.cluster import KMeans
import random
import matplotlib.pyplot as plt
'''
	FUNTIONS START
'''
'''
KMEANS CLUSTERING OF DATA
INPUT - data-TRAINING DATA cluster_count=NO OF COUNTS OR (BASIS_FUNCTIONS_COUNT - 1)
OUTPUT - NAMED TUPLE CONTAINING centers-centroids of the clusters, spreads-covariance matrix for sub-dataset of each cluster
'''
def k_means_cluster_data(data,cluster_count):

    clusterGenerator = KMeans(n_clusters=cluster_count);
    clusterGenerator.fit(data);

    centers=clusterGenerator.cluster_centers_;

    clusterIdvsClusterMap = {i: np.where(clusterGenerator.labels_ == i)[0] for i in range(clusterGenerator.n_clusters)};
    spreads = [];

    for cluster in clusterIdvsClusterMap:
        clusterDataRows = clusterIdvsClusterMap[cluster];
        subDataSet = data[clusterDataRows];
        subdf = pd.DataFrame(subDataSet);
        covarianceMat = subdf.cov().as_matrix();
        spreads.append(covarianceMat);
    centers = np.asarray(centers).T
    spreads = np.asarray(spreads)
    kmeans = collections.namedtuple('kmeans', ['centers', 'spreads'])
    return kmeans(centers,spreads)

'''
DESIGN MATRIX COMPUTATION
INPUT - X-TRAINING DATA, centers-CLUSTER CENTERS, spreads-COVARIANCE MATRIX FOR EACH CLUSTER
OUTPUT - MxN DESIGN MATRIX [M-BASIS FUNCTION COUNT, N-NO OF DATA POINTS]
'''
def compute_design_matrix(X, centers, spreads): # use broadcast
    N, D = X.shape
    # shape = [M, 1, D]
    centers = centers.T
    centers = centers[:, np.newaxis, :]
    # shape = [M, D, D]
    X = X[np.newaxis, :, :]
    basis_func_outputs = np.exp(
    np. sum(
    (np.matmul(X - centers, spreads)) * (X - centers), axis=2
    ) / (-2) ).T
    # insert ones to the 1st col
    return np.insert(basis_func_outputs, 0, 1, axis=1)

'''
DESIGN MATRIX COMPUTATION
INPUT - L2_lambda-NORMALIZAITON FACTOR, design_matrix-MxN DESIGN MATRIX, output_data-OUTPUT FOR EACH DATAROW IN X
OUTPUT - Mx1 CLOSED FORM SOULTION - WEIGHTS OF THE MODEL
'''
def closed_form_sol(L2_lambda, design_matrix, output_data):
    return np.linalg.solve(
        L2_lambda * np.identity(design_matrix.shape[1]) + np.matmul(design_matrix.T, design_matrix),
        np.matmul(design_matrix.T, output_data)
    )

'''
DESIGN MATRIX COMPUTATION
INPUT - learning_rate-LEARNING RATE, minibatch_size-BATCH ROW COUNT PROCESSED FOR EACH UPDATE, num_epochs=NO OF ITERATIONS
	L2_lambda-NORMALIZAITON FACTOR, design_matrix-MxN DESIGN MATRIX, output_data-OUTPUT FOR EACH DATAROW IN X
	initial_weights-Mx1 WEIGHTS MATRIX TO START THE CALCULATION - OPTIONAL PARAMETER
OUTPUT - Mx1 SOULTION - WEIGHTS OF THE MODEL
'''
def SGD_sol(learning_rate, minibatch_size,num_epochs, L2_lambda,design_matrix,output_data, initial_weights=None):
    N, M = design_matrix.shape
    error_arr = []
# You can try different mini-batch size size
# Using minibatch_size = N is equivalent to standard gradient descent
# Using minibatch_size = 1 is equivalent to stochastic gradient descent
# In this case, minibatch_size = N is better
    weights = initial_weights
    if weights == None:
        weights = np.zeros([1, M])
# The more epochs the higher training accuracy. When set to 1000000,
# weights will be very close to closed_form_weights. But this is unnecessary
    for epoch in range(num_epochs):
        for i in range(math.ceil(N / minibatch_size)):
            lower_bound = i * minibatch_size
            upper_bound = min((i+1)*minibatch_size, N)
            Phi = design_matrix[lower_bound : upper_bound, :] 
            t = output_data[lower_bound : upper_bound, :]
            E_D = np.matmul((np.matmul(Phi, weights.T)-t).T,Phi)
            E = (E_D + L2_lambda * weights) / minibatch_size
            weights = weights - learning_rate * E 
            error_arr.append(np.mean(((E)**2)**(0.5)))
    if initial_weights == None:
    	plt.plot(error_arr)
    	plt.xlabel('Epoch')
    	plt.ylabel('Mean Error')
    	plt.show()
    return weights

'''
GRADIENT DESCENT WRAPPER
INPUT - initial_weights-Mx1 WEIGHTS MATRIX TO START THE CALCULATION, training_data=TRAIN DATA, training_output_data=TRAIN DATA OUTPUT
	 learning_rate-LEARNING RATE, minibatch_size-BATCH ROW COUNT PROCESSED FOR EACH UPDATE, descent_iterations=NO OF ITERATIONS
	L2_lambda-NORMALIZAITON FACTOR, centers-CLUSTER CENTERS, spreads-COVARIANCE MATRIX FOR EACH CLUSTER
OUTPUT - Mx1 SOULTION - WEIGHTS OF THE MODEL
'''
def train_model_sgd(initial_weights,training_data,training_output_data,
                                 centers,spreads,L2_lambda,learning_rate,
                                 batch_size,descent_iterations):
    design_matrix = compute_design_matrix(training_data, centers, spreads)
    return SGD_sol(learning_rate=learning_rate, minibatch_size=batch_size,
            num_epochs=descent_iterations, L2_lambda=L2_lambda, 
            design_matrix=design_matrix, output_data=training_output_data,
                  initial_weights=initial_weights)

'''GRADIENT DESCENT WRAPPER
INPUT - training_data=TRAIN DATA, training_output_data=TRAIN DATA OUTPUT, weights-MODEL WEIGHTS, centers-CLUSTER CENTERS, spreads-COVARIANCE MATRIX FOR EACH CLUSTER
OUTPUT - ROOT MEAN SQUARE ERROR
'''
def compute_error(data,output,weights,centers,spreads):
    phi = compute_design_matrix(data,centers,spreads).T;
    mse = (np.sum((output - (np.matmul(weights.T,phi)))**2))/2;
    return ((2*mse)**(0.5))/data.shape[0]

'''EARLY STOPPING ALGORITHM IMPLEMENTATION
INPUT - initial_weights-Mx1 WEIGHTS MATRIX TO START THE CALCULATION, training_data=TRAIN DATA, training_output_data=TRAIN DATA OUTPUT
		validation_data=VALIDATION DATA, validation_output_data=VALIDATION DATA OUTPUT,centers-CLUSTER CENTERS, spreads-COVARIANCE MATRIX FOR EACH CLUSTER
	 	learning_rate-LEARNING RATE, minibatch_size-BATCH ROW COUNT PROCESSED FOR EACH UPDATE, descent_iterations=NO OF ITERATIONS
	L2_lambda-NORMALIZAITON FACTOR, patience-PATIENCE PARAMETER - NO OF ITERATIONS TO WAIT IF THE WAITS UPDATE TO THE SAME VALUE
OUTPUT - Mx1 SOULTION - WEIGHTS OF THE MODEL
'''
def early_stop_implementation_sgd(training_data,training_output_data,
                                  validation_data,validation_output_data,
                                  centers,spreads, L2_lambda, 
                                  learning_rate, batch_size, 
                                  descent_iterations,patience):
    current_validation_error = float("inf")
    noChangeIterations = 0
    trainingSteps = 0
    current_weights = np.zeros([1, spreads.shape[0]+1])
    weights = current_weights
    best_training_steps = 0
    max_iterations = 10000
    validation_error_map={}
    while noChangeIterations < patience and trainingSteps < max_iterations:
        weights = train_model_sgd(initial_weights=weights, 
                                  training_data=training_data,
                                 training_output_data=training_output_data,
                                 centers=centers,
                                 spreads=spreads,
                                 L2_lambda=L2_lambda,
                                 learning_rate=learning_rate,
                                 batch_size = batch_size,
                                 descent_iterations = descent_iterations)
        trainingSteps = trainingSteps + descent_iterations
        validation_error = compute_error(validation_data,validation_output_data,weights.T,centers,spreads)
        validation_error_map[trainingSteps] = validation_error
        if validation_error < current_validation_error:
            noChangeIterations = 0
            current_weights = weights
            best_training_steps = trainingSteps
            current_validation_error = validation_error
        else:
            noChangeIterations = noChangeIterations + 1
    x_axis = list(validation_error_map.keys())
    y_axis = list(validation_error_map.values())
    x_axis_new = np.linspace(min(x_axis),max(x_axis),500)
    y_axis = sp.interpolate.spline(x_axis,y_axis,x_axis_new)
    plt.plot(x_axis_new,y_axis)
    plt.xlabel('Epoch')
    plt.ylabel('Error')
    plt.show()
    sgd_es = collections.namedtuple('sgd_es', ['validation_error', 'weights','error_val_map'])
    return sgd_es(current_validation_error,current_weights,validation_error_map)

'''
CLOSED FORM SOLUTION WRAPPER
INPUT - training_data=TRAIN DATA, training_output_data=TRAIN DATA OUTPUT, validation_data=VALIDATION DATA, validation_output_data=VALIDATION DATA OUTPUT
	L2_lambda-NORMALIZAITON FACTOR, centers-CLUSTER CENTERS, spreads-COVARIANCE MATRIX FOR EACH CLUSTER
OUTPUT - Mx1 SOULTION - WEIGHTS OF THE MODEL
'''
def closed_form_implementation(training_data,training_output_data,
                                  validation_data,validation_output_data,
                                  #testing_data,testing_output_data,
                                  centers,spreads,L2_lambda):
	design_matrix = compute_design_matrix(training_data, centers, spreads)
	weights = closed_form_sol(L2_lambda=L2_lambda, design_matrix=design_matrix,output_data=training_output_data)
	validation_error = compute_error(validation_data,validation_output_data,weights,centers,spreads)
	closed_form_model = collections.namedtuple('closed_form_model', ['weights','validation_error'])
	return closed_form_model(weights,validation_error)

'''
GRADIENT DESCENT WRAPPER
INPUT - training_data=TRAIN DATA, training_output_data=TRAIN DATA OUTPUT, validation_data=VALIDATION DATA, validation_output_data=VALIDATION DATA OUTPUT
	 learning_rate-LEARNING RATE, batch_size-BATCH ROW COUNT PROCESSED FOR EACH UPDATE, descent_iterations=NO OF ITERATIONS
	L2_lambda-NORMALIZAITON FACTOR, centers-CLUSTER CENTERS, spreads-COVARIANCE MATRIX FOR EACH CLUSTER
OUTPUT - Mx1 SOULTION - WEIGHTS OF THE MODEL
'''
def sgd_implementation(training_data,training_output_data,
                                  validation_data,validation_output_data,
                                  centers,spreads, L2_lambda, 
                                  learning_rate, batch_size, 
                                  descent_iterations):
	design_matrix = compute_design_matrix(training_data, centers, spreads)
	weights=SGD_sol(learning_rate=learning_rate, minibatch_size=batch_size,
				num_epochs=descent_iterations, L2_lambda=L2_lambda, design_matrix=design_matrix, output_data=training_output_data)
	validation_error = compute_error(validation_data,validation_output_data,weights.T,centers,spreads)
	sgd_model = collections.namedtuple('sgd_model', ['weights','validation_error'])
	return sgd_model(weights,validation_error)

'''
EVALUATION OF MODELS
INPUT - training_data=TRAIN DATA, training_output_data=TRAIN DATA OUTPUT, validation_data=VALIDATION DATA, validation_output_data=VALIDATION DATA OUTPUT
		test_data=TEST DATA, test_output_data=TEST DATA OUTPUT learning_rate-LEARNING RATE, batch_size-BATCH ROW COUNT PROCESSED FOR EACH UPDATE, 
		descent_iterations=NO OF ITERATIONS, L2_lambda-NORMALIZAITON FACTOR, centers-CLUSTER CENTERS, spreads-COVARIANCE MATRIX FOR EACH CLUSTER
OUTPUT - Mx1 SOULTION - WEIGHTS OF THE MODEL
'''
def evaluate_linear_regression_model(train_data, train_output_data,
									 validation_data, validation_output_data,
									 test_data, test_output_data,
									 basis_fn_count,L2_lambda,learning_rate):
	trainingDataSize = train_data.shape[0];
	batch_size = trainingDataSize
	descent_iterations = 100 
	patience = 10

	kmeans = k_means_cluster_data(trainingData,basis_fn_count-1)
	centers = kmeans.centers
	spreads = kmeans.spreads

	closed_form_model = closed_form_implementation(train_data,train_output_data,validation_data,validation_output_data,
	                                  centers,spreads, L2_lambda)
	closed_form_testing_error = compute_error(test_data,test_output_data,closed_form_model.weights,centers,spreads)

	sgd_model = sgd_implementation(train_data,train_output_data,validation_data,validation_output_data,
	                                  centers,spreads, L2_lambda, learning_rate, batch_size, descent_iterations)
	sgd_model_error = compute_error(test_data,test_output_data,sgd_model.weights.T,centers,spreads)

	sgd_es = early_stop_implementation_sgd(train_data,train_output_data,validation_data,validation_output_data,
	                                  centers,spreads, L2_lambda, learning_rate, batch_size, descent_iterations, patience)
	sgd_es_error = compute_error(test_data,test_output_data,sgd_es.weights.T,centers,spreads)

	print('------------------------------------------------------------------')
	print('Closed-From Solution')
	print('Weights = ')
	print(closed_form_model.weights.T)
	print('Validation error = ' + str(closed_form_model.validation_error))
	print('Testing error = ' + str(closed_form_testing_error))

	print('------------------------------------------------------------------')
	print('Gradient Descent Solution')
	print('Weights = ')
	print(sgd_model.weights)
	print('Validation error = ' + str(sgd_model.validation_error))
	print('Testing error = ' + str(sgd_model_error))

	print('------------------------------------------------------------------')
	print('Early Stop Algorithm Gradient Descent Solution')
	print('Weights = ')
	print(sgd_es.weights)
	print('Validation error = ' + str(sgd_es.validation_error))
	print('Testing error = ' + str(sgd_es_error))
	print('------------------------------------------------------------------')

	model_parameters = collections.namedtuple('model_parameters', ['closed_form_weights','sgd_weights','early_stop_sgd_weights'])
	return model_parameters(closed_form_model.weights,sgd_model.weights,sgd_es.weights)

'''
	FUNTIONS END
'''

L2_lambda = 0.1
learning_rate = 0.1
basis_fn_count = 5

print('basis_fn_count = ' + str(basis_fn_count) +  '  learning_rate = ' + str(learning_rate) + '  lambda = ' +str(L2_lambda))

#LeToR Dataset
letorData = np.genfromtxt('./Querylevelnorm_X.csv', delimiter=',');
rank = np.genfromtxt('./Querylevelnorm_t.csv', delimiter=',').reshape([-1, 1]);
#letorData = letorData[:100];

dataSetSize = letorData.shape[0];
trainingDataSize = math.ceil(0.8*dataSetSize);
validationDataSize = math.ceil(0.1*dataSetSize);

#Step to reduce overfitting
indices = np.arange(0,dataSetSize)
random.shuffle(indices)

#seggreation of dataset
trainingData = letorData[indices[:trainingDataSize]];
trainingOutput = rank[indices[:trainingDataSize]];
validationData = letorData[indices[trainingDataSize:(trainingDataSize+validationDataSize)]];
validationOutput = rank[indices[trainingDataSize:(trainingDataSize+validationDataSize)]];
testingData = letorData[indices[(trainingDataSize+validationDataSize):]];
testOutput = rank[indices[(trainingDataSize+validationDataSize):]];

evaluate_linear_regression_model(train_data=trainingData,train_output_data=trainingOutput,
									validation_data=validationData, validation_output_data=validationOutput,
									test_data=testingData, test_output_data=testOutput,
									basis_fn_count=basis_fn_count,L2_lambda=L2_lambda,learning_rate=learning_rate)

#Synthetic Data
basis_fn_count = 3
synthetic_data = np.loadtxt('./input.csv', delimiter=',');
output = np.loadtxt('./output.csv', delimiter=',').reshape([-1, 1]);

dataSetSize = synthetic_data.shape[0];
trainingDataSize = math.ceil(0.8*dataSetSize);
validationDataSize = math.ceil(0.1*dataSetSize);

#seggreation of dataset
trainingData = synthetic_data[:trainingDataSize];
trainingOutput = output[:trainingDataSize];
validationData = synthetic_data[trainingDataSize:(trainingDataSize+validationDataSize)];
validationOutput = output[trainingDataSize:(trainingDataSize+validationDataSize)];
testingData = synthetic_data[(trainingDataSize+validationDataSize):];
testOutput = output[(trainingDataSize+validationDataSize):];


evaluate_linear_regression_model(train_data=trainingData,train_output_data=trainingOutput,
									validation_data=validationData, validation_output_data=validationOutput,
									test_data=testingData, test_output_data=testOutput,
									basis_fn_count=basis_fn_count,L2_lambda=L2_lambda,learning_rate=learning_rate)


###END OF PROGRAM
